# pylint: disable=E1101
from sqlalchemy import and_
from patch_tracking.database import db
from patch_tracking.database.models import Tracking, Issue


def create_tracking(data):
    version_control = data.get("version_control")
    scm_repo = data.get('scm_repo')
    scm_branch = data.get('scm_branch')
    scm_commit = data.get('scm_commit')
    repo = data.get('repo')
    branch = data.get('branch')
    enabled = data.get('enabled')
    tracking = Tracking(version_control, scm_repo, scm_branch, scm_commit, repo, branch, enabled)
    db.session.add(tracking)
    db.session.commit()


def update_tracking(data):
    repo = data.get('repo')
    branch = data.get('branch')
    tracking = Tracking.query.filter(and_(Tracking.repo == repo, Tracking.branch == branch)).one()
    tracking.version_control = data.get("version_control")
    tracking.scm_repo = data.get('scm_repo')
    tracking.scm_branch = data.get('scm_branch')
    tracking.scm_commit = data.get('scm_commit')
    tracking.enabeld = data.get('enabled')
    db.session.add(tracking)
    db.session.commit()


def delete_tracking(id_):
    post = Tracking.query.filter(Tracking.id == id_).one()
    db.session.delete(post)
    db.session.commit()


def create_issue(data):
    issue = data.get('issue')
    repo = data.get('repo')
    branch = data.get('branch')
    issue_ = Issue(issue, repo, branch)
    db.session.add(issue_)
    db.session.commit()


def update_issue(data):
    issue = data.get('issue')
    issue_ = Issue.query.filter(Issue.issue == issue).one()
    issue_.issue = data.get('issue')
    db.session.add(issue_)
    db.session.commit()


def delete_issue(issue):
    issue_ = Issue.query.filter(Issue.issue == issue).one()
    db.session.delete(issue_)
    db.session.commit()
