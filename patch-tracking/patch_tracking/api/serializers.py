from flask_restx import fields
from patch_tracking.api.restplus import api

tracking = api.model(
    'Tracking', {
        'version_control': fields.String(required=True, description=''),
        'scm_repo': fields.String(required=True, description=''),
        'scm_branch': fields.String(required=True, description=''),
        'scm_commit': fields.String(required=True, description=''),
        'repo': fields.String(required=True, description=''),
        'branch': fields.String(required=True, description=''),
        'enabled': fields.Boolean(required=True, description=''),
    }
)

pagination = api.model(
    'A page of results', {
        'page': fields.Integer(description='Number of this page of results'),
        'pages': fields.Integer(description='Total number of pages of results'),
        'per_page': fields.Integer(description='Number of items per page of results'),
        'total': fields.Integer(description='Total number of results'),
    }
)

page_of_tracking = api.inherit('Page of tracking', pagination, {'items': fields.List(fields.Nested(tracking))})

issue = api.model(
    'Issue', {
        'issue': fields.String(required=True, description=''),
        'repo': fields.String(required=True, description=''),
        'branch': fields.String(required=True, description=''),
    }
)
