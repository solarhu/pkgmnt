# pylint: disable=R0201
import logging
from flask_restx import Resource
from patch_tracking.api.serializers import issue
from patch_tracking.api.restplus import api
from patch_tracking.database.models import Issue
from patch_tracking.api.parsers import issue_request

log = logging.getLogger(__name__)

ns = api.namespace('issue', description='Operations related to issue')


@ns.route('')
class IssueCollection(Resource):
    @api.expect(issue_request)
    @api.marshal_list_with(issue)
    def get(self):
        """
        Returns list of issue.
        """
        issues = Issue.query.all()
        return [{'issue': item.issue, 'repo': item.repo, 'branch': item.branch} for item in issues]

    # @api.response(201, 'Issue successfully created.')
    # @api.expect(issue)
    # def post(self):
    #     """
    #     Creates a new issue.
    #     """
    #     data = request.json
    #     create_issue(data)
    #     return None, 201
    #
    # @api.expect(issue)
    # @api.response(204, 'Issue successfully updated.')
    # def put(self):
    #     """
    #     Updates a issue.
    #
    #     Use this method to change the data of issue.
    #
    #     * Send a JSON object with the new data in the request body.
    #
    #     ```
    #     {
    #       "issue": "issue"，
    #       "tracking_id": "New Tracking ID"
    #     }
    #     ```
    #
    #     * Specify the ID of the category to modify in the request URL path.
    #     """
    #     data = request.json
    #     update_issue(data)
    #     return None, 204
    #
