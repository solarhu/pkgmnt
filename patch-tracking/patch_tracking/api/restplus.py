import logging
import traceback
from flask_restx import Api
from sqlalchemy.orm.exc import NoResultFound

logger = logging.getLogger(__name__)

api = Api(version='1.0', title='Patch Tracking API', description='A simple demonstration ')


@api.errorhandler
def default_error_handler(err):
    message = 'An unhandled exception occurred.'
    logger.exception(err)

    return {'message': message}, 500


@api.errorhandler(NoResultFound)
def database_not_found_error_handler(err):
    logger.warning(err)
    logger.warning(traceback.format_exc())
    return {'message': 'A database result was required but none was found.'}, 404
