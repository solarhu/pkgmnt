# Flask settings
FLASK_SERVER_NAME = '127.0.0.1:5001'

# SQLAlchemy settings
SQLALCHEMY_DATABASE_URI = 'sqlite:///db.sqlite'

# GitHub API settings
GITHUB_ACCESS_TOKEN = '<GitHub token>'

# Gitee API settings
GITEE_ACCESS_TOKEN = '<Gitee token>'
GITEE_OWNER = "<Gitee Owner>"

# Time interval
SCAN_DB_INTERVAL = 3600
TRACKING_JOB_INTERVAL = 3600
