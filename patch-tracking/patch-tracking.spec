%define name patch-tracking
%define version 1.0.0
%define unmangled_version 1.0.0
%define release 1

Summary: This is a tool for automatically tracking upstream repository code patches
Name: %{name}
Version: %{version}
Release: %{release}
Source0: %{name}-%{unmangled_version}.tar.gz
License: Mulan PSL v2
Group: Development/Libraries
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-buildroot
Prefix: %{_prefix}
BuildArch: noarch
Vendor: ChenYanpan <chenyanpan@huawei.com>
Url: https://openeuler.org/zh/

%description
This is a tool for automatically tracking upstream repository code patches

Requires: python3.7 python3-flask python3-sqlalchemy python3-requests


%prep
%setup -n %{name}-%{unmangled_version} -n %{name}-%{unmangled_version}

%build
python3 setup.py build

%install
python3 setup.py install --single-version-externally-managed -O1 --root=$RPM_BUILD_ROOT --record=INSTALLED_FILES

%post
sed -i "s|'logging.conf'|'/etc/patch-tracking/logging.conf'|" %{python3_sitelib}/patch_tracking/app.py
sed -i "s|db.sqlite|/var/run/patch-tracking/db.sqlite|" %{python3_sitelib}/patch_tracking/settings.py
ln -sf %{python3_sitelib}/patch_tracking/settings.py /etc/patch-tracking/settings.conf
chmod +x /usr/bin/patch-tracking-cli
chmod +x /usr/bin/patch-tracking

%clean
rm -rf $RPM_BUILD_ROOT

%files -f INSTALLED_FILES
%defattr(-,root,root)
