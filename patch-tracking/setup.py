from setuptools import setup, find_packages

setup(
    name='patch-tracking',
    version='1.0.0',
    packages=find_packages(),
    url='https://openeuler.org/zh/',
    license='Mulan PSL v2',
    author='ChenYanpan',
    author_email='chenyanpan@huawei.com',
    description='This is a tool for automatically tracking upstream repository code patches',
    install_requires=['requests', 'flask', 'flask-restx', 'Flask-SQLAlchemy', 'Flask-APScheduler'],
    data_files=[
        ('/etc/patch-tracking/', ['patch_tracking/logging.conf']),
        ('/var/run/patch-tracking/', ['patch_tracking/db.sqlite']),
        ('/usr/bin/', ['patch_tracking/cli/patch-tracking-cli']),
        ('/usr/bin/', ['patch_tracking/patch-tracking']),
    ],
)
