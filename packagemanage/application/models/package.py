from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy import Column, Integer, String, DateTime, Boolean, ForeignKey
from sqlalchemy.orm import relationship
from sqlalchemy.orm.session import Session
from libs.dbutils.sqlalchemy_helper import DBHelper


class src_pack(DBHelper.BASE):
    '''
    functional description:Source package model
    modify record:
    '''

    __tablename__ = 'src_pack'

    id = Column(Integer, primary_key=True)

    name = Column(String(200), unique=True, nullable=False)

    version = Column(String(50), nullable=False)

    license = Column(String(50), nullable=False)

    sourceURL = Column(String(200), nullable=True)

    downloadURL = Column(String(200), nullable=True)

    Maintaniner = Column(String(50), nullable=True)

    MaintainLevel = Column(String(20), nullable=True)


class bin_pack(DBHelper.BASE):
    '''
    functional description:Binary package data
    modify record:
    '''
    __tablename__ = 'bin_pack'

    id = Column(Integer, primary_key=True)

    name = Column(String(200), unique=True, nullable=False)

    version = Column(String(50), nullable=False)

    srcIDkey = Column(Integer, ForeignKey('src_pack.id'))

    src_pack = relationship('src_pack', backref="bin_pack")


class pack_requires(DBHelper.BASE):
    '''
    functional description:
    modify record:
    '''

    __tablename__ = 'pack_requires'

    id = Column(Integer, primary_key=True)

    name = Column(String(200), nullable=False)

    depProIDkey = Column(Integer)

    srcIDkey = Column(Integer, ForeignKey('src_pack.id'), nullable=True)

    binIDkey = Column(Integer, ForeignKey('bin_pack.id'), nullable=True)


class pack_provides(DBHelper.BASE):
    '''
    functional description:
    modify record:
    '''
    __tablename__ = 'pack_provides'

    id = Column(Integer, primary_key=True)

    name = Column(String(200), nullable=False)

    binIDkey = Column(Integer, ForeignKey('bin_pack.id'))
