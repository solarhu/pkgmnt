from . import view

urls = [
    # Get all packages' info
    (view.Packages, '/packages'),

    # Query and update a package info
    (view.SinglePack, '/packages/findByPackName'),

    # Query a package's install depend(support querying in one or more databases)
    (view.InstallDepend, '/packages/findInstallDepend'),

    # Query a package's build depend(support querying in one or more databases)

    (view.BuildDepend, '/packages/findBuildDepend'),

    # Query a package's all dependencies including install and build depend(support quering a binary or source package in one or more databases)
    (view.SelfDepend, '/packages/findSelfDepend'),

    # Query a package's all be dependencies including be installed and built depend
    (view.BeDepend, '/packages/findBeDepend'),

    # Get all imported databases, import new databases and update existed databases
    (view.Repodatas, '/repodatas')
]
